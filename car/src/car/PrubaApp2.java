/**
 * 
 */
package car;

/**
 * @author Carlos 
 *
 */
public class PrubaApp2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int operador1=20;
		int operador2=15;
		int resultado=0;
		resultado= operador1+operador2; //resultado=35
		System.out.println("resultado"+resultado);
		resultado= operador1-operador2; //resultado=5
		System.out.println("resultado"+resultado);
		resultado= operador2-operador1; //resultado=-5
		System.out.println("resultado"+resultado);
		resultado= operador1*operador2; //resultado=300
		System.out.println("resultado"+resultado);
		resultado= operador1/operador2; //resultado=1 (como es int no incluye decimales)
		System.out.println("resultado"+resultado);
		resultado= operador1%operador2; //resultado=5(el resto de la division)
		System.out.println("resultado"+resultado);
	}

}
